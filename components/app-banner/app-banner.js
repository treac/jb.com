Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        res: [{
                name: "联想笔记本电脑 拯救者Y9000P 11代i7八核旗舰电竞游戏本 16英寸高刷高色域设计师办公学生本 八核旗舰定制I7-11800H 16G 1TB RTX3060-6G光追独显 钛晶灰",
                price: "10599",
                count: "65",
                shop: "联想电脑授权专卖店",
                image: "../../assets/img/pc.png"
            },
            {
                name: "Apple 苹果iPhone 13 Pro Max (A2644)【12期免息可选】全网通5G手机 远峰蓝色 256GB【官方标配】",
                price: "9789",
                count: "2028",
                shop: "京北电竞手机官方旗舰店",
                image: "../../assets/img/phone.png"
            },
            {
                name: "beats Studio3苹果蓝牙耳机头戴降噪游戏耳麦 Beats耳机 魅影灰 咨询优惠",
                price: "1898",
                count: "117",
                shop: "Beats旗舰店",
                image: "../../assets/img/beats.png"
            },
            {
                name: "微软（Microsoft）日版 Xbox Series X 次世代4K主机 家用家庭高清电视游戏机 1TB 全面升级体验更佳",
                price: "5299",
                count: "4804",
                shop: "数码海外京东自营专区",
                image: "../../assets/img/xbox.png",
            },
        ],
        bannerIndex: 0
    },

    /**
     * 组件的方法列表
     */
    methods: {
        bannerChange(event) {
            this.setData({
                bannerIndex: event.detail.current
            })
        },
        pointChange(event) {
            this.setData({
                bannerIndex: event.target.dataset.index
            })
        }
    }
})