const AppData = getApp().globalData; // 全局数据
let cartList = AppData.cartList; // 购物车列表数据

Page({
    data: {
        res: [], // 购物车数据
        totalNum: 0, // 商品总数
        totalPrice: 0, // 商品总价
        allChecked: true, // 全选框
        isNull: true, // 购物车是否有商品
        isEdit: false // 购物车是否为编辑状态
    },
    // 跳转到首页
    toHome() {
        wx.switchTab({
            url: '../home/home',
        })
    },
    // 跳转到支付页
    toPay() {
        // 缓存支付数据
        wx.setStorageSync('payment', this.data.res);
        // 缓存商品总价
        wx.setStorageSync('totalPrice', this.data.totalPrice)
        
        // 跳转页面
        wx.navigateTo({
          url: '/pages/pay/pay?type=cart',
        })
    },
    // 购物车管理
    cartEdit() {
        let isedit = this.data.isEdit; // 记录购物车编辑状态
        isedit = !isedit; // 点击切换编辑状态
        // 重新定义数据
        this.setData({
            isEdit: isedit
        })
    },
    // 增加商品数量
    addNum(event) {
        let index = event.target.dataset.index; // 获取事件对象的下标
        let num = this.data.res[index].num; // 获取商品原本数量
        num++; // 增加商品数量

        // 重新定义数据
        let obj = {};
        let key = 'res[' + index + '].num';
        obj[key] = num;
        this.setData(obj);
        // 保存到全局数据
        cartList = this.data.res;

        // 调用商品数量函数
        this.totalNum();
        // 调用商品总价函数
        this.totalPrice();
    },
    // 减少商品数量
    reduceNum(event) {
        let index = event.target.dataset.index; // 获取事件对象的下标
        let num = this.data.res[index].num; // 获取商品原本数量
        let res = this.data.res; // 接收购物车数据
        let isnull = this.data.isNull; // 记录购物车是否为空

        // 商品数量大于0
        if (num > 0) {
            num--; // 减少商品数量
        }

        // 重新定义数据
        let obj = {};
        let key = 'res[' + index + '].num';
        obj[key] = num;
        this.setData(obj);

        // 商品数量为0
        if (num <= 0) {
            // 商品数量为0
            res.splice(index, 1);
        }
        // 判断购物车是否为空
        if (res.length == 0) {
            isnull = true;
        }
        // 重新定义数据
        this.setData({
            isNull: isnull,
            res: res
        })

        // 保存到全局数据
        cartList = this.data.res;

        // 调用商品数量函数
        this.totalNum();
        // 调用商品总价函数
        this.totalPrice();
    },
    // 删除商品
    delData(event) {
        let num = this.data.totalNum; // 接收商品总数数据
        let res = this.data.res; // 接收购物车数据
        let isnull = false; // 记录购物车是否为空

        // 判断商品是否选中
        for (let i = res.length-1; i >= 0; i--) {
            if (res[i].checked) {
                res.splice(i, 1); // 删除选中的商品
            }
        }

        // 判断购物车是否为空
        if (res.length == 0) {
            isnull = true;
        }

        // 重新定义数据
        this.setData({
            res: res,
            totalNum: num,
            isNull: isnull
        })
        // 保存到全局数据
        cartList = this.data.res;

        // 调用商品总价函数
        this.totalPrice();
    },
    // 计算商品总数
    totalNum() {
        let num = 0; // 商品数量
        this.data.res.forEach((item) => {
            num += item.num
        })
        // 重新定义数据
        this.setData({
            totalNum: num
        })
    },
    // 计算商品总价
    totalPrice() {
        let price = 0;
        this.data.res.forEach((item) => {
            if (item.checked == true) {
                price += item.price * item.num
            }
        })
        // 重新定义数据
        this.setData({
            totalPrice: price
        })
    },
    // 判断商品是否勾选
    checking(event) {
        let index = event.target.dataset.index; // 获取事件对象下标值
        let value = event.detail.value[0]; // 获取事件对象选中状态，选中Value为select
        let checked = cartList[index].checked; // 记录事件对象勾选框状态

        // 判断事件对象是否勾选
        if (value == "select") {
            checked = true;
        } else {
            checked = false;
        }
        // 重新定义数据
        let obj = {};
        let key = 'res[' + index + '].checked';
        obj[key] = checked;
        this.setData(obj);
        // 保存到全局数据
        cartList = this.data.res;

        let allchecked = true; // 记录全选框状态
        // 判断是否有商品未勾选
        this.data.res.forEach(item => {
            if (item.checked == false) {
                allchecked = false;
            }
        })
        // 重新定义数据
        this.setData({
            allChecked: allchecked
        })

        // 调用商品总价函数
        this.totalPrice();
    },
    // 判断商品是否全选
    allChecking(event) {
        let value = event.detail.value; // 记录全选框状态
        let res = this.data.res; // 接收购物车数据
        // 判断购物车是否全选
        res.forEach(item => {
            if (value == "all-select") {
                item.checked = true;
            } else {
                item.checked = false;
            }
        })
        // 重新定义数据
        this.setData({
            res: res
        })
        // 保存到全局数据
        cartList = this.data.res;

        // 调用商品总价函数
        this.totalPrice();
    },
    // 页面显示
    onShow() {
        let res = cartList; // 接收全局数据传值
        let isnull = true; // 记录购物车是否为空
        if (cartList.length != 0) {
            isnull = false;
        }

        // 判断是否有商品未选中
        let allchecked = true;
        res.forEach(item => {
            if (item.checked == false) {
                allchecked = false;
            }
        })

        // 重新定义数据
        this.setData({
            res: res,
            isNull: isnull,
            allChecking: allchecked
        })
        // 保存到全局数据
        cartList = this.data.res;

        // 调用商品数量函数
        this.totalNum();
        // 调用商品总价函数
        this.totalPrice();
    },
    // 页面隐藏
    onHide() {
        let isedit = this.data.isEdit; // 获取购物车编辑状态
        isedit = false; // 恢复默认
        // 重新定义数据
        this.setData({
            isEdit: isedit
        })
    }
})