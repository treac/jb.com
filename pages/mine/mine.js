const AppData = getApp().globalData; // 全局数据
let userInfo = AppData.userInfo;

Page({
  data: {
    username: "", // 用户名字
    useravatar: "", // 用户头像
    isLogin: false, // 判断是否登录
    order: [
      {
        image: "../../assets/ui/order.png",
        text: "全部订单"
      },
      {
        image: "../../assets/ui/pay.png",
        text: "待付款"
      },
      {
        image: "../../assets/ui/take.png",
        text: "待收货"
      }
    ]
  },
  // 获取用户信息
  getInfo() {
    // 将this赋值给一个变量that
    let that = this;
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserProfile({
            desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success(res) {
              // 修改data数据
              that.setData({
                username:res.userInfo.nickName,
                useravatar:res.userInfo.avatarUrl,
                isLogin: true
              })
              userInfo.push(res.userInfo.avatarUrl);
              console.log(userInfo);
            }
          })
        }
      }
    })
  },
  // 跳转到订单页面
  toOrder(event) {
    let index = event.target.dataset.index;
    wx.navigateTo({
      url: '/pages/order/order?index=' + index,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})