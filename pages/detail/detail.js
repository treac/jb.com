const AppData = getApp().globalData; // 全局数据
let cartList = AppData.cartList; // 购物车列表数据
let userInfo = AppData.userInfo; // 购物车列表数据

Page({
    data: {
        res: {}, // 详情页数据
        isLike: false, // 商品收藏状态
        isClick: false, // 按下按钮状态
        mouseX: 0, // 鼠标横坐标
        mouseY: 0, // 鼠标纵坐标
        totalNum: 0, // 商品总数
        isCartNull: true // 判断购物车是否有商品
    },
    // 添加收藏
    addLike() {
        let res = this.data.res; // 接收商品数据
        let name = this.data.res.name; // 接收商品名称
        let islike = false; // 记录商品收藏状态

        // 判断是否登录
        if (userInfo.length == 0) {
            // 未登录，将跳转到我的页面
            wx.switchTab({
                url: '/pages/mine/mine',
            })
            wx.showToast({
                title: '请先登录',
                icon: 'error',
                duration: 3000
            })
        } else {
            // 已登录，可进行收藏
            if (wx.getStorageSync(name)) {
                let goods = wx.getStorageSync(name)
                islike = goods.like;
            } else {
                islike = res.like;
            }
            // 收藏或取反
            islike = !islike;
            // 重新定义数据
            let obj = {};
            let key = 'res.like';
            obj[key] = islike;
            this.setData(obj)
            this.setData({
                isLike: islike
            })
            wx.setStorageSync(name, res);
        }


    },
    // 添加到购物车
    addCart() {
        let res = this.data.res; // 接收详情页数据
        let isnull = true; // 记录购物车状态
        let isData = false; // 记录商品是否已存在
        let num = 0; // 记录商品数量

        // 判断是否登录
        if (userInfo.length == 0) {
            // 未登录，将跳转到我的页面
            wx.switchTab({
                url: '/pages/mine/mine',
            })
            wx.showToast({
                title: '请先登录',
                icon: 'error',
                duration: 3000
            })
        } else {
            // 已登录，可加入购物车

            // 判断购物车是否有商品
            if (cartList.length == 0) {
                // 购物车没有商品，直接添加商品
                cartList.unshift(res); // 将当前商品数据添加到全局数据中
                num++; // 商品总数增加
                isnull = false; // 购物车存在商品
            } else {
                // 购物车已有商品
                // 商品相同，数量增加
                cartList.forEach((item) => {
                    // 判断商品是否相同
                    if (res.name == item.name) {
                        item.num++; // 商品数量增加
                        isData = true; // 购物车有相同商品
                        isnull = false; // 购物车存在商品
                    }
                })
                // 商品不相同，添加商品
                if (!isData) {
                    cartList.unshift(res); // 将当前商品数据添加到全局数据中
                    isnull = false; // 购物车存在商品
                }
                // 计算商品总数
                cartList.forEach(item => {
                    num += item.num; // 商品总数增加
                })
            }

            // 商品总数超过99
            if (num > 99) {
                num = "99+"
            }

            // 重新定义数据
            this.setData({
                totalNum: num,
                isCartNull: isnull
            })
        }
    },
    // 跳转到购物车
    toCart() {
        wx.switchTab({
            url: '/pages/cart/cart',
        })
    },
    // 跳转到支付页
    toPay() {
        // 缓存支付数据
        wx.setStorageSync('payment', [this.data.res]);
        // 缓存商品总价
        wx.setStorageSync('totalPrice', this.data.res.price);

        // 判断是否登录
        if (userInfo.length == 0) {
            // 未登录，将跳转到我的页面
            wx.switchTab({
                url: '/pages/mine/mine',
            })
            wx.showToast({
                title: '请先登录',
                icon: 'error',
                duration: 3000
            })
        } else {
            // 已登录，可进行购买
            wx.navigateTo({
                url: '/pages/pay/pay?type=detail',
            })
        }
    },
    // 跳转到客服页
    toSupport() {
        // 判断是否登录
        if (userInfo.length == 0) {
            // 未登录，将跳转到我的页面
            wx.switchTab({
                url: '/pages/mine/mine',
            })
            wx.showToast({
                title: '请先登录',
                icon: 'error',
                duration: 3000
            })
        } else {
            // 已登录，可进入客服页面
            wx.navigateTo({
                url: '/pages/support/support',
            })
        }
    },
    // 按下按钮
    btnStart() {
        let isclick = this.data.isClick;
        isclick = true;
        this.setData({
            isClick: isclick
        })
    },
    // 拖动按钮
    btnMove(event) {
        let isclick = this.data.isClick; // 接收鼠标点击状态
        let mousex = 0; // 记录鼠标横坐标
        let mousey = 0; // 记录鼠标纵坐标
        if (isclick) {
            mousex = event.touches[0].pageX - 34;
            mousey = event.touches[0].pageY - 34;
            this.setData({
                mouseX: mousex,
                mouseY: mousey,
            })
        }
    },
    // 松开按钮
    btnEnd() {
        let isclick = this.data.isClick;
        if (isclick) {
            isclick = false;
            this.setData({
                isClick: isclick
            })
        }
    },
    // 页面加载
    onLoad(option) {
        let obj = {}
        obj.name = option.name; // 商品名称
        obj.price = option.price; // 商品价格
        obj.count = option.count; // 商品购买人数
        obj.shop = option.shop; // 店铺名称
        obj.image = option.image; // 商品图片
        obj.checked = true; // 商品勾选
        obj.num = 1; // 商品数量
        obj.like = this.data.isLike; // 商品收藏
        this.setData({
            res: obj
        })
    },
    // 页面显示
    onShow() {
        let res = this.data.res; // 接收商品数据
        let name = this.data.res.name; // 接收商品名称
        let islike = this.data.isLike; // 记录商品收藏状态
        let num = 0; // 记录商品总数;
        let isnull = true; // 记录购物车状态
        let winWid = wx.getSystemInfoSync().windowWidth - 50; // 获取屏幕宽度
        let winHei = wx.getSystemInfoSync().windowHeight - 150; // 获取屏幕高度

        // 判断购物车是否有商品
        if (cartList.length != 0) {
            cartList.forEach(item => {
                num += item.num;
                isnull = false; // 购物车存在商品
            })
        } else {
            num = 0;
            isnull = true; // 购物车不存在商品
        }

        // 判断是否已存在缓存数据
        if (wx.getStorageSync(name)) {
            // 获取缓存数据
            let goods = wx.getStorageSync(name)
            islike = goods.like;
        } else {
            // 获取商品数据
            islike = res.like;
        }

        // 重新定义数据
        this.setData({
            isLike: islike,
            mouseX: winWid,
            mouseY: winHei,
            totalNum: num,
            isCartNull: isnull
        })
    }
})