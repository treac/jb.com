const AppData = getApp().globalData; // 全局数据
let cartList = AppData.cartList; // 购物车列表数据

Page({
    data: {
        res: [], // 商品订单数据
        user: "张三", // 收货人
        tel: "186****680", // 收货电话
        adress: "广东省广州市天河区珠吉路菜鸟驿站", // 收货地址
        totalPrice: 0, // 记录商品总价
        type: "" // 判断商品购买路径
    },
    // 处理订单
    order(key, value, type) {
        // 获取要提交的订单
        let data = this.data.res;
        // 判断订单是否已有数据
        if (value.length == 0) {
            // 没有数据，直接将页面数据添加到订单
            data.forEach(item=>{
                item.type = type;
            })
            wx.setStorageSync(key, data)
        } else {
            // 已有数据，将页面数据遍历添加到订单数组中，再添加到订单
            data.forEach(item => {
                value.unshift(item)
            })
            value.forEach(item=>{
                item.type = type;
            })
            wx.setStorageSync(key, value)
        }
    },
    // 提交订单
    pay() {
        let that = this; // 声明指针变量
        // let allData = wx.getStorageSync('allData'); // 获取全部订单数据
        let payData = wx.getStorageSync('payData'); // 获取待付款数据
        let recData = wx.getStorageSync('recData'); // 获取待收货数据

        // 提交订单，弹出提示框
        wx.showModal({
            title: "￥" + this.data.totalPrice, // 提示框标题
            content: "确认支付？", // 提示框内容
            success(res) {
                if (res.confirm) { // 点击确认
                    wx.navigateTo({
                        url: '/pages/order/order?index=2',
                    })
                    // 保存到全部订单
                    // that.order('allData', allData, "receive");
                    // 保存到待收货
                    that.order('recData', recData, "receive");
                } else if (res.cancel) { // 点击取消
                    wx.navigateTo({
                        url: '/pages/order/order?index=1',
                    })
                    // 保存到全部订单
                    // that.order('allData', allData, "payment");
                    // 保存到待付款
                    that.order('payData', payData, "payment");
                }

                // 判断是否从购物车结算
                if (that.data.type == "cart") {
                    // 提交订单后，将已结算的商品从购物车删除
                    for (let i = cartList.length - 1; i >= 0; i--) {
                        if (cartList[i].checked) {
                            cartList.splice(i, 1);
                        }
                    }
                }
            }
        })
    },
    // 页面加载
    onLoad(option) {
        let type = option.type; // 接收跳转类型

        // 重新定义数据
        this.setData({
            type: type
        })
    },
    // 页面显示
    onShow() {
        let res = wx.getStorageSync('payment'); // 接收支付数据
        let price = wx.getStorageSync('totalPrice'); // 接收商品总价

        // 重新定义数据
        this.setData({
            res: res,
            totalPrice: price
        })
    }
})