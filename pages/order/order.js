Page({
    data: {
        navData: ["全部", "待付款", "待收货"],
        navIndex: 0, // 导航栏索引值
        allOrder: [], // 全部订单
        payment: [], // 待付款订单
        receive: [], // 待收货订单
        hei: "" // 屏幕高度
    },
    // 切换导航栏
    navChange(event) {
        this.setData({
            navIndex: event.target.dataset.index
        })
    },
    // 切换内容
    pageChange(event) {
        this.setData({
            navIndex: event.detail.current
        })
    },
    // 判断是哪种
    typeTap(event) {
        let type = event.target.dataset.type;
        if (type == "payment") {
            this.payment(event);
        } else {
            this.receive();
        }
    },
    // 订单付款
    payment(event) {
        let price = event.target.dataset.price;
        wx.showModal({
            title: "￥" + price, // 提示框标题
            content: "确认支付？", // 提示框内容
            success(res) {
                if (res.confirm) {
                    wx.showLoading({
                        title: '加载中',
                    })
                    setTimeout(function () {
                        wx.hideLoading()
                        wx.showToast({
                            title: '支付失败',
                            icon: 'error',
                            duration: 1500
                        })
                    }, 1000)
                } else if (res.cancel) {
                    wx.showToast({
                        title: '取消支付',
                        icon: 'error',
                        duration: 1500
                    })
                }
            }
        })
    },
    // 确认收货
    receive() {
        wx.showModal({
            title: "确认是否到货", // 提示框标题
            content: "确认到货？", // 提示框内容
            success(res) {
                if (res.confirm) {
                    wx.showLoading({
                        title: '加载中',
                    })
                    setTimeout(function () {
                        wx.hideLoading()
                        wx.showToast({
                            title: '收货失败',
                            icon: 'error',
                            duration: 1500
                        })
                    }, 1000)
                } else if (res.cancel) {
                    wx.showToast({
                        title: '取消收货',
                        icon: 'error',
                        duration: 1500
                    })
                }
            }
        })
    },
    // 页面加载
    onLoad(option) {
        if (option.length != 0) {
            this.setData({
                navIndex: option.index
            })
        }
        // 获取屏幕高度
        this.setData({
            hei: (wx.getSystemInfoSync().windowHeight) * 2
        })
    },
    // 页面显示
    onShow() {
        let allOrder = ""; // 记录全部订单数据
        let payData = wx.getStorageSync('payData'); // 接收未付款数据
        let recData = wx.getStorageSync('recData'); // 接收未收货数据

        // 判断订单数据是否为空
        if (payData.length == 0) {
            allOrder = recData;
        } else if (recData.length == 0) {
            allOrder = payData;
        } else {
            allOrder = payData.concat(recData)
        }

        // 重新定义数据
        this.setData({
            allOrder: allOrder,
            payment: payData,
            receive: recData
        })
    },
    onUnload() {
        wx.switchTab({
            url: '/pages/mine/mine',
        })
    }
})