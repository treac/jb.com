const AppData = getApp().globalData; // 全局数据
let userInfo = AppData.userInfo; // 购物车列表数据

Page({
    data: {
        msg: "", // 输入框值
        myMsg: [], // 我的消息
        myImg: "", // 我的头像
        supMsg: ["你好"], // 客服消息
        msg1: "好的",
        msg2: "再见"
    },
    sendMsg() {
        // 判断输入框是否有值
        if (this.data.msg.length != 0) {
            let mymsg = this.data.myMsg; // 我的消息
            mymsg.push(this.data.msg) // 记录我的消息
            // 发送信息后清空输入框
            this.setData({
                myMsg: mymsg,
                msg: ""
            })

            let supMsg = this.data.supMsg; // 回复的消息
            let len = this.data.myMsg.length; // 我的消息数组长度
            supMsg.push(this.data.msg1) // 记录客服消息

            // 判断是否有特殊语句
            if (mymsg[len - 1] == "再见") {
                supMsg.splice(supMsg.length-1, 1);
                supMsg.push(this.data.msg2);
            }
            this.setData({
                supMsg: supMsg
            })
        } else {
            // 输入框未输入内容
            wx.showToast({
                title: '请输入内容',
                icon: 'error',
                duration: 1000
            })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        // 获取用户头像
        if (userInfo) {
            this.setData({
                myImg: userInfo[0]
            })
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})