Page({
    data: {
        value: "", // 输入框值
        arr: [], // 搜索数据
        res: [], // 搜索结果
        his: [], // 历史记录
        isHis: false, // 是否有历史记录
    },
    // 获取搜索数据
    getSearch() {
        // 判断输入框是否有值
        if (this.data.value.length != 0) {
            // 输入框有值
            let that = this;
            // 商品搜索接口
            wx.request({
                url: 'http://api.mkstone.club/api/v1/open/jd/searchGoodsSimple',
                method: "GET",
                data: {
                    content: that.data.value,
                    appkey: "vsr93CaKrmXA4PY7Z"
                },
                success(res) {
                    if (res.statusCode == 200) {
                        // 判断是否存在搜索结果
                        if (res.data.Data) {
                            // 存在搜索结果，接收给商品数据
                            res.data.Data.forEach(item => {
                                let obj = {};
                                obj.name = item.sku_name;
                                obj.shop = item.shop_name;
                                obj.price = item.price;
                                obj.image = item.image_url;
                                obj.count = item.in_order_count_30_days;
                                that.data.arr.push(obj);
                            })
                            // 重新定义搜索数据
                            that.setData({
                                arr: that.data.arr,
                            })
                            // 打印搜索数据
                            // console.log(that.data.arr);
                        } else {
                            // 不存在搜索结果
                            wx.showToast({
                                title: '未找到该商品',
                                icon: 'error',
                                duration: 1500
                            })
                        }
                    }
                },
                fail(err) {
                    // 请求失败
                    wx.showToast({
                        title: '请打开调试来使用搜索功能',
                        icon: 'error',
                        duration: 5000
                    })
                }
            })
            // 获取历史记录
            this.data.his.unshift(this.data.value);
            if (this.data.his.length > 10) {
                this.data.his.splice(this.data.his.length - 1, 1);
            }
            // 重新定义数据
            this.setData({
                arr: [], // 重置搜索结果
                his: this.data.his, // 记录历史记录
                isHis: true, // 显示历史记录
                value: "" // 重置输入框
            })
        } else {
            // 输入框为空
            this.data.arr = []; // 重置搜索结果
            console.log("请输入商品信息");
        }
    },
    // 重置搜索结果
    resetInput() {
        if (this.data.value.length == 0) {
            console.log("重置输入框");
        }
    },
    // 历史记录搜索
    historySearch(event) {
        this.setData({
            value: event.currentTarget.dataset.value
        })
        this.getSearch();
    },
    // 删除历史记录
    deleteHistory() {
        this.setData({
            his: [],
            isHis: false, // 隐藏历史记录
        })
    },
    onLoad() {
        // console.log(this.data.isHis);
    }
})